# Changelog
All notable changes to this project will be documented in this file.

## 0.8.9 – 2020-12-20
### Added
- Nextcloud 20 compatibility (#12)
- Add changelog information to distribution (#14)
- Add error handling during parsing and display errors to the user (#13)
- Label printing of single member

### Changed
- Update library dependencies

## 0.7.2 – 2020-09-05
### Added
- Add usage instructions to `info.xml` and to the web interface (#11).
- Update library dependencies

## 0.7.1 – 2020-09-01
### Added
- Install FPDF via composer
- Fix export error when salutation is empty
- Update library dependencies

## 0.7.0 – 2020-08-10
### Added
- Update JS Dependencies
- Make parsing of SPG files more robust (#5).

## 0.6.0 – 2020-05-28
### Added
- Improve UI: align it with Nextcloud design
- Show that there is no data aivalable to display.

## 0.5.1 – 2020-05-28
### Added
- Fix app signing (#2)

## 0.5.0 – 2020-01-01
### Added
- Parse `mitgl.dat` file as members
- Group members by related member id
- Show members in web interface sorted by districts
- Download members as CSV
